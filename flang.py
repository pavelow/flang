#! /usr/bin/python


def add(a):
    acc = 0
    a = list(map(int, a))
    for i in a:
        acc += i
    return acc


def sub(a):
    a = list(map(int, a))
    acc = a[0]
    for i in a[1:]:
        acc -= i
    return acc


def mul(a):
    a = list(map(int, a))
    acc = a[0]
    for i in a[1:]:
        acc *= i
    return acc


func = {}
func['+'] = add
func['-'] = sub
func['*'] = mul


def tokenize(line):
    tokens = []
    token = ""
    brack = 0
    for c in line:
        if c == ')':
            brack -= 1
            if(len(token) > 0):
                if(brack > 0):
                    token += ")"*brack
                tokens.append(token)
                token = ""
        if c == '(':
            brack += 1
        if(brack == 1):
            if(c not in "()"):
                if(c != ' '):
                    token += c
                else:
                    tokens.append(token)
                    token = ""

        if(brack > 1):
            if(c not in ")"):
                token += c
    if(brack != 0):
        tokens = "Error: Brackets"

    return [i for i in tokens if i != '']



def eval(line):
    t = tokenize(line)

    for i in range(len(t)):
        if t[i][0] == '(':
            t[i] = eval(t[i])

    return func[t[0]](t[1:])


print("Welcome to, uh.. flang")
while True:
    i = input("> ")
    if(len(i) > 0):
        print(eval(i))

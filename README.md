# fLang
A toy functional language very similar to lisp, implemented in python.  
Witten mainly to sate my curiosity.


## Features
- Addition `(+ 1 2 3 4)`
- Subtraction `(- 1 2)`
- Multiplication `(* 1 2)`
- Nested statements `(* (+ 2 (- 3 2)) 5)`

## Todo
- Function definitions
- ??
